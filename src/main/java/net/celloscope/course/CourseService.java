package net.celloscope.course;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    public List<Course> getAllCourses(String topicId) {

        List<Course> AllCoursesLists = new ArrayList<>();
        courseRepository.findByTopicId(topicId)
                .forEach(AllCoursesLists::add);
        return AllCoursesLists;
    }

    public Course getCourse(String id) {
        return courseRepository.findById(id).get();
    }

    public void addCourse(Course course) {
        courseRepository.save(course);
    }

    public void updateCourse(Course course) {
        courseRepository.save(course);
    }

    public void deleteCourse(String id) {
        courseRepository.deleteById(id);
    }
}


/*
POST
curl -X POST http://localhost:8080/topics -d '{"id":"4","name":"Chemistry","description":"EFG"}' -H 'Content-Type: application/json'
 * */

/*
PUT
curl -X PUT http://localhost:8080/topics -d '{"id":"4","name":"Chemistry","description":"EFG"}' -H 'Content-Type: application/json'
 * */

/*
DELETE
curl -X DELETE http://localhost:8080/topics/2 -H 'Accept: application/json'
 */
