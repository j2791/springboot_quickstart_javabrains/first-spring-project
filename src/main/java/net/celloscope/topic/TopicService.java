package net.celloscope.topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TopicService {

    @Autowired
    private TopicRepository topicRepository;

    public List<Topic> getAllTopics() {
        List<Topic> AllTopicsLists = new ArrayList<>();
        topicRepository.findAll()
                .forEach(AllTopicsLists::add);
        return AllTopicsLists;
    }

    public Topic getTopic(String id) {
        return topicRepository.findById(id).get();

    }

    public void addTopic(Topic topic) {
        topicRepository.save(topic);
    }

    public void updateTopic(Topic topic, String id) {
        topicRepository.save(topic);
    }

    public void deleteTopic(String id) {
        topicRepository.deleteById(id);
    }
}


/*
POST
curl -X POST http://localhost:8080/topics -d '{"id":"4","name":"Chemistry","description":"EFG"}' -H 'Content-Type: application/json'
 * */

/*
PUT
curl -X PUT http://localhost:8080/topics -d '{"id":"4","name":"Chemistry","description":"EFG"}' -H 'Content-Type: application/json'
 * */

/*
DELETE
curl -X DELETE http://localhost:8080/topics/2 -H 'Accept: application/json'
 */
